# Use an official Python runtime as a parent image
FROM python:3.6.4

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD ./app /app

# Copy the requirements into the container at /etc
COPY ./requirements.txt /etc

# Install any needed packages specified in requirements.txt
RUN pip install --upgrade pip && pip3 install -r /etc/requirements.txt

# Make port 8080 available to the world outside this container
EXPOSE 5000

## References for environment variable
# ENV TOKEN = ""
# ENV TXT = ""
# ENV TIMEOUT = ""
# ENV PORT_TCP = ""
# ENV PORT_HTTP = ""
# ENV PORT_SERVER = ""
# ENV ENDPOINT = ""


# Run app.py when the container launches
CMD ["python", "/app/app.py"]
