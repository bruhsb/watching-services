# Service Monitor:

This project provides service monitoring over TCP and HTTP connections

## Components

- O Código da aplicação está na pasta ./app
- As libs necessárias estão indicadas no arquivo requirements.txt
- Também disponível um Dockerfile para a criação da imagem docker.

## Utilização

### Criação da Imagem Docker

Basta utilizar o comando padrão para a criação da imagem

```
docker build -t brunosb/service_monitor:latest .
```

### Utilizando a aplicação localmente

É possível a execução direta do código, com o interpretador python ou pela imagem docker criada:

Executando diretamente

```
/usr/bin/python3 ./watching-services/app/app.py
```

Utilizando Docker

```
docker run -d -p 5000:5000 brunosb/service_monitor:latest
```

### Parâmetros e Configurações

As configurações da aplicação é feito pelas seguintes variáveis de ambiente:

Variável    | Descrição                                    | Valor Padrão
----------- | -------------------------------------------- | ------------------
ENDPOINT    | URL para conexão com o serviço remoto        | tonto.cloudwalk.io
PORT_HTTP   | Porta HTTP do serviço remoto                 | 8080
PORT_SERVER | Porta do serviço local                       | 5000
PORT_TCP    | Porta TCP do serviço remoto                  | 3000
TIMEOUT     | tempo limite de espera da conexão            | 10 Segundos
TOKEN       | token para autenticação com o serviço remoto | < _sensível_ >
TXT         | Informação a ser enviada ao serviço remoto   | EXAMPLE_TXT

### Execução

Ao iniciar a aplicação, pela saída padrão podemos observar as seguintes informações:

```
* Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```

Ao receber uma requisição http em `http://IP_APP:5000/healthcheck`, o serviço irá validar a disponibilidade do serviço remoto:

```
curl localhost:5000/healthcheck
```

output

```json
{
    "hostname": "dell-inspiron-7000",
    "status": "success",
    "timestamp": 1584454875.416055,
    "results": [
        {
            "checker": "http_connect",
            "output": "HTTP Connection ok!",
            "passed": true,
            "timestamp": 1584454875.3886576,
            "expires": 1584454902.3886576,
            "response_time": 0.272293
        },
        {
            "checker": "tcp_connect",
            "output": "TCP Connection ok!",
            "passed": true,
            "timestamp": 1584454875.4159772,
            "expires": 1584454902.4159772,
            "response_time": 0.027279
        }
    ]
}
```

Na saída padrão da aplicação é possível ver o seguinte retorno:

```
HTTP Test Connection
status code: 200
response text: CLOUDWALK EXAMPLE_TXT

TCP Test Connection
Response: b'CLOUDWALK EXAMPLE_TXT\r\n'
127.0.0.1 - - [17/Mar/2020 11:19:41] "GET /healthcheck HTTP/1.1" 200 -
```

### TODO

- [x] Check http connection
- [x] Check tcp connection
- [x] timeout
  - [x] tcp
  - [x] http
  - [x] variable

- [x] healthcheck

- [x] Dockerfile / Image

- [x] Build pelo Gitlab-CI
- [x] Versionamento pelo Semantic-Release

- [ ] The service must have 5 parameters:
  - [ ] email address to receive notifications,
  - [ ] check interval (seconds),
  - [ ] timeout (seconds),
  - [ ] health threshold (consecutive successes) and an unhealth threshold (consecutive failures).

- [ ] The parameters must be configured externaly via commands or config files.

- [ ] The service is considered healthy when you get the expected response consecutively for n times as parameterized.

- [ ] The service is considered unhealthy by any unexpected error or response for n times as parameterized.
- [ ] Google App Engine Standard Platform with terraform
- [ ] CI - Infra
