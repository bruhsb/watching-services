#!flask/bin/python

# importing library
from flask import Flask
import requests
import socket                       # https://docs.python.org/2/library/socket.html
import os
from healthcheck import HealthCheck # https://pypi.org/project/py-healthcheck/

# enviroment's variables
TOKEN = os.getenv("TOKEN", "6eb718f846c6d303ed8054cdf7ccdb18c821de18")
TXT = os.getenv("TXT", "EXAMPLE_TXT")

TIMEOUT = int(os.getenv("TIMEOUT", "10"))

PORT_TCP = int(os.getenv("PORT_TCP", "3000"))
PORT_HTTP = os.getenv("PORT_HTTP","8080")
PORT_SERVER = os.getenv("PORT_SERVER","5000")
ENDPOINT = os.getenv("ENDPOINT","tonto.cloudwalk.io")

URL = f"http://{ENDPOINT}:{PORT_HTTP}/"

# function to test http conection
def http_connect():

    ploads = {'auth': TOKEN, 'buf': TXT}

    r = requests.get(URL, params=ploads, timeout=10)
    if str(r.status_code) == "200":
        print("HTTP Test Connection")
        print("  status code: %s" % str(r.status_code))
        print("  response text: %s" % r.text)
        return True, "HTTP Connection ok!"

    print("Erro!")
    print("status code: %s" % str(r.status_code))
    return False, "HTTP Connection Failed!"


# function to test TCP conection
def tcp_connect():

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    sock.connect((ENDPOINT, PORT_TCP))
    # sock.settimeout(10.0)
    data = f"auth {TOKEN}\r\n{TXT}\r\n"
    sock.sendall(data.encode('utf-8'))
    response = sock.recv(4096)
    if "CLOUDWALK" in str(response):
        sock.close()
        print("TCP Test Connection")
        print("  Response: %s" % response)
        return True, "TCP Connection ok!"

    sock.close()
    return False, "TCP Connection Failed!"

# Init Flask
app = Flask(__name__)

# root
@app.route('/')
def index():
    return "Hi! :P"

# Init HealthCheck
health = HealthCheck()
health.add_check(http_connect)
health.add_check(tcp_connect)
app.add_url_rule("/healthcheck", "healthcheck", view_func=lambda: health.run())

# WebServer
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)